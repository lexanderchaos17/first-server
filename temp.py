import requests
import hashlib
import sqlite3 as sql

db_client = sql.connect('UserData.db')
cur = db_client.cursor()
cur.execute(
    "CREATE TABLE IF NOT EXISTS user (name TEXT, passwd TEXT, city TEXT, cityID INTEGER)"
)
db_client.commit()


def login():
    name = input('Enter your name >> ')
    password = input('Come up with a password for the account >> ')
    p_hash = hashlib.md5(password.encode('utf-8'))
    password = p_hash.hexdigest()
    city = input('Enter you`r city. For now time work only Moscow >> ')
    req_loc = requests.get("https://www.metaweather.com/api/location/search/?query={}".format(city))
    loc = req_loc.json()
    cityID = loc[0]["woeid"]
    user = (name, password, city, cityID)

    cur.execute('INSERT INTO user VALUES (?,?,?,?)', user)

    r = requests.post('http://127.0.0.1:5000//login', json=user)
    anw = r.json()
    return anw


def reg():
    name = input('Enter your name >> ')
    password = input('Come up with a password for the account >> ')
    p_hash = hashlib.md5(password.encode('utf-8'))
    password = p_hash.hexdigest()
    city = input('For now time work only Moscow >> ')
    req_loc = requests.get("https://www.metaweather.com/api/location/search/?query={}".format(city))
    loc = req_loc.json()
    cityID = loc[0]["woeid"]
    user = (name, password, city, cityID)

    cur.execute('INSERT INTO user VALUES (?,?,?,?)', user)

    r = requests.post('http://127.0.0.1:5000//registratio', json=user)
    anw = r.json()
    return anw


def weather():
    user = []
    cur.execute('SELECT name, cityID FROM user')
    temp = cur.fetchall()

    for date in temp:
        user.append(temp[0])

    print(user)
    r3 = requests.get('http://127.0.0.1:5000//weather', json=user)
    temp_anw = r3.json()
    anw = 'Max temperature {} min temperature {}'.format(temp_anw[0], temp_anw[1])
    return anw


def check():
    r2 = requests.get('http://127.0.0.1:5000//checkALL')
    anw = r2.json()
    return anw


print('>>Welcome my friend')
print('>>Now I`ii tell you basic rules')
print('>>You can answer using Yes/No. If you want to close connection just enter quit')

que = input('>>You here for the first? (Yes/No) >> ')
if que == 'Yes' or que == 'y' or que == 'yes' or que == 'Y':
    print('>>Start registration on server')
    while True:
        anw = reg()
        server_anw1 = 'This name is already occupied. Please think of another name'
        server_anw2 = 'You enter a empty password pleas try something else. For example the letter (a)'
        if anw[0] == server_anw1 or anw[0] == server_anw2:
            print(">>Please check you name/password and try again")
            continue
        elif anw[0] == 'NOW YOU A WITH US':
            print('>>Registration successes')
            print('>>Connect to server')
            print(anw[0])
            print('>>Now you can choose by two methods: ', end='')
            print('>>Weather or Check all user on server')
            break
        break

    while True:
        choose = input('>>Weather or checkUser. If you want close window enter quit >> ')
        if choose == 'Weather' or choose == 'weather':
            print(weather())
        elif choose == 'Check' or choose == 'checkUser' or choose == 'check':
            print(check())
        elif choose == 'Quit' or 'quit':
            print('>>Goodbye my friend')
            cur.execute('DELETE FROM users')
            break
        else:
            print('>>Please check if you entered the word correctly')



elif que == 'No' or que == 'n' or que == 'no' or que == 'N':
    print('>>Try to connect with server')
    while True:
        anw = login()
        print('             Step1')
        print(anw)
        if anw[0] == 'This name is already occupied. Please think of another name':
            print(anw[0])
            continue
        elif anw[0] == 'You enter a empty password':
            print(anw[0])
            continue
        elif anw[0] == 'GOOD TO SEE YOU AGAIN':
            print('>>Connect to server')
            print('>>' + anw[0])
            print('>>Now you can choose by two methods: ')
            print('>>Weather or Check all user on server')
            break
    while True:
        choose = input('>>Weather or checkUser. If you want close window enter quit >> ')
        if choose == 'Weather' or choose == 'weather':
            print(weather())
        elif choose == 'Check' or choose == 'checkUser' or choose == 'check':
            print(check())
        elif choose == 'Quit' or 'quit':
            print('>>Goodbye my friend')
            cur.execute('DELETE FROM users')
            log = False
            continue
        else:
            print('>>Please check if you entered the word correctly')

elif que == 'Quit' or 'quit':
    print('>>Glad to see you. Enjoy you`r day')
