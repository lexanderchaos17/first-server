import hashlib
from flask import Flask, request, jsonify
from datetime import datetime
import requests
import sqlite3 as sql

app = Flask(__name__)
dt = datetime.now()

db_app = sql.connect('appData.db')
cur = db_app.cursor()
cur.execute(
    "CREATE TABLE IF NOT EXISTS users (id INTEGER, name TEXT, passwd TEXT, city TEXT, cityID INTEGER)"
)
db_app.commit()

# Для корректной работы сервера, необходимо создать пользователя admin, раскоментив с 20 по 22 строку
# после чего закоментировать их обратно.


# admin = (1, 'admin', 'admin', 'Moscow', 2122265)
# cur.execute('INSERT INTO users VALUES(?, ?, ?, ?, ?)', admin)
# db_app.commit()
# cur.execute('SELECT * FROM users')
# inf = cur.fetchall()
# print(inf)
# # cur.execute('DELETE FROM users')
# # db_app.commit()
# # cur.execute('SELECT * FROM users')
# # inf = cur.fetchall()
# # print(inf)


@app.route('/')
def hello():
    print('HELLO NEW USER')
    return {}

@app.route('/checkALL')
def check():
    db_app = sql.connect('appData.db')
    cur = db_app.cursor()
    cur.execute('SELECT * FROM users')
    inf = cur.fetchall()
    print(inf)
    return jsonify(inf)

@app.route('/checkTypePass')
def checkTypePass():
    db_app = sql.connect('appData.db')
    cur = db_app.cursor()

    cur.execute('SELECT passwd FROM users')
    users = cur.fetchall()
    print(type(users))
    for user in users:
        print(type(user[0]))
        return {}


@app.route('/login', methods=['POST'])
def login():
    new_user = request.json
    name = new_user[0]
    passw = new_user[1]

    check_user = False
    check_passw = False

    db_app = sql.connect('appData.db')
    cur = db_app.cursor()

    cur.execute('SELECT name, passwd FROM users')
    users = cur.fetchall()

    for user in users:
        if name == user[0]:
            print(name, name[0])
            check_user = True
            print('check user')
            if passw == user[1]:
                check_passw = True
                break

    if check_passw == True and check_user == True:
        return jsonify(['GOOD TO SEE YOU AGAIN'])
    else:
        return jsonify(['WRONG PASSWORD OR LOGIN'])


@app.route("/registratio", methods=['POST'])
def registratio():
    db_app = sql.connect('appData.db')
    cur = db_app.cursor()

    data = []

    new_user = request.json

    if bool(new_user[0]):  # проверка имени н_пользователя
        cur.execute('SELECT name FROM users')
        names = cur.fetchall()
        for name in names:
            if new_user[0] == name[0]:
                return jsonify(['This name is already occupied. Please think of another name'])
        else:
            data.append(new_user[0])

    if str(new_user[1]) != 'd41d8cd98f00b204e9800998ecf8427e':  # проверка пустоты пароля
        data.append(new_user[1])
    else:
        return jsonify(['You enter a empty password pleas try something else. For example the letter (a)'])

    if bool(new_user[2]) != False:  # проверка пустоты города
        data.append(new_user[2])
    else:
        data.append('Moscow')

    if bool(new_user[3]) != False:  # проверка пустоты id города
        data.append(new_user[3])
    else:
        data.append(2122265)
    cur.execute('SELECT id FROM users')
    ids = cur.fetchall()
    new_id = ids[-1][-1] + 1
    data.insert(0, new_id)
    print(data)
    cur.execute('INSERT INTO users VALUES(?, ?, ?, ?, ?)', data)
    db_app.commit()
    cur.execute('SELECT * FROM users')
    inf = cur.fetchall()
    return jsonify(['NOW YOU A WITH US'])

@app.route("/weather", methods=['GET'])
def weather():
    db_app = sql.connect('appData.db')
    cur = db_app.cursor()
    cur.execute('SELECT name, city, cityID FROM users')
    cites = cur.fetchall()
    user = request.json
    print(user)

    for city in cites:
        if user[0][0] == city[0]:
            print('step1')
            time = dt.strftime('%Y/%m/%d')
            r1 = requests.get(("https://www.metaweather.com//api/location/{}/{}".format(int(city[2]), time)))
            anw1 = r1.json()
            w = (int(anw1[0]['max_temp']), int(anw1[0]['min_temp']))
            return jsonify(w)
    return {}


if __name__ == '__main__':
    app.run(debug=True)
    
